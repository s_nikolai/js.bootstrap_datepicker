js.bootstrap_datepicker
=======================

.. contents::

Introduction
------------

This library packages `Datepicker for Bootstrap`_ for `fanstatic`_.

.. _`fanstatic`: http://fanstatic.org
.. _`Datepicker for Bootstrap`: http://www.eyecon.ro/bootstrap-datepicker/

This requires integration between your web framework and ``fanstatic``,
and making sure that the original resources (shipped in the ``resources``
directory in ``js.bootstrap_datepicker``) are published to some URL.
